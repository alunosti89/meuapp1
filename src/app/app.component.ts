import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  public appPages = [
    {
      title: 'Inbox',
      url: '/folder/Inbox',
      icon: 'mail'
    },
    {
      title: 'Card',
      url: 'card',
      icon: 'card'
    },
    {
      title: 'Navegação',
      url: 'navegacao1',
      icon: 'paper-plane'
    }
    ,
    {
      title: 'Botões',
      url: 'buttons', // rota
      icon: 'apps'
    } ,
    {
      title: 'Badges',
      url: 'badges', // rota
      icon: 'apps'
    } ,
    {
      title: 'CheckBox',
      url: 'checkbox', // rota - caminho para chegar nesta pasta
      icon: 'checkmark'
    }
    ,
    {
      title: 'Datetime',
      url: 'datetime', // rota - caminho para chegar nesta pasta
      icon: 'calendar'
    }
    ,
    {
      title: 'Inputs',
      url: 'inputs', // rota - caminho para chegar nesta pasta
      icon: 'list'
    },
    {
      title: 'FAB',
      url: 'fab', // rota - caminho para chegar nesta pasta
      icon: 'list'
    }
    ,
    {
      title: 'Grid',
      url: 'grid', // rota - caminho para chegar nesta pasta
      icon: 'grid'
    },
    {
      title: 'IMC',
      url: 'imc', // rota - caminho para chegar nesta pasta
      icon: 'calculator'
    },
    {
      title: 'CEP',
      url: 'cep', // rota - caminho para chegar nesta pasta
      icon: 'map'
    }




  ];
  public alunos = ['Renato','Gabriel O', 'Gabriel R', 'Eduardo', 'Wellington', 'Ramon'];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
    const path = window.location.pathname.split('folder/')[1];
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }
  }
}
