import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CepService {
// pesquise verbos HTTP!!!!!!!!!!!!!!!!!!!1
  constructor(private http: HttpClient) { }

  obterEndereco(cep: string){
    let url = 'http://www.cep.republicavirtual.com.br/web_cep.php?cep='+ cep +'&formato=json';
    return this.http.get(url).toPromise();
  }
}
