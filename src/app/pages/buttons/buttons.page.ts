import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-buttons',
  templateUrl: './buttons.page.html',
  styleUrls: ['./buttons.page.scss'],
})
export class ButtonsPage implements OnInit {

  constructor(private alertController: AlertController) { }

  ngOnInit() {
  }

  async chamarAlerta() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Fique esperto',
      subHeader: 'Maravilha',
      message: 'Te vejo na próxima aula aqui no Teams.',
      buttons: ['OK']
    });

    await alert.present();
  }

}
