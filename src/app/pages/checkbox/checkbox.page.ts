import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.page.html',
  styleUrls: ['./checkbox.page.scss'],
})
export class CheckboxPage implements OnInit {
meses: any[]=[
  {nomeMes: 'Janeiro',valor: 1, marcado:true},
  {nomeMes: 'Fevereiro',valor: 2, marcado:false},
  {nomeMes: 'Março',valor: 3, marcado:true},
  {nomeMes: 'Abril',valor: 4, marcado:true},
  {nomeMes: 'Maio',valor: 5, marcado:false},
  {nomeMes: 'Junho',valor: 6, marcado:false},
]
  constructor() { }

  ngOnInit() {
  }
  exibirMeses(){
    // somente meses marcados
    let marcado = this.meses.filter(a => true == a.marcado);//lamba
    console.log(marcado);
    // persistir dados localmente = localStorage
    
  }
}
