import { Component, OnInit } from '@angular/core';
import { CepService } from 'src/app/services/cep.service';

@Component({
  selector: 'app-cep',
  templateUrl: './cep.page.html',
  styleUrls: ['./cep.page.scss'],
})
export class CepPage implements OnInit {
 cep: string;
 resultado: any = {
   cep: '',
   logradouro: '', 
   bairro: '', 
   tipo_logradouro:''};
   local: string='';
  constructor(private servcep: CepService) { }

  ngOnInit() {
  }

  consultarCep(){
    this.servcep.obterEndereco(this.cep)
    .then((json)=>{
      //console.log(json)
      this.resultado = json;
      this.resultado.logradouro  =this.resultado.tipo_logradouro +' '+ this.resultado.logradouro
    })
    .catch();
  }

}
