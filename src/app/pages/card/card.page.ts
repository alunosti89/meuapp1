import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.page.html',
  styleUrls: ['./card.page.scss'],
})
export class CardPage implements OnInit {
public cards = [ 
  {img : 'floripa.jpg',
  sub: 'Minhas viagens',
  tit:'Férias em Florianópolis',
  cont:'No mês de julho de 2019 visitamos a ilha. Depos de 985 km de estrada.'
  },
  {img : 'cataratas.jpg',
  sub: 'Minhas viagens',
  tit:'Férias em Foz do Iguaçu',
  cont:'No mês de julho de 2017 visitamos as cataratas do rio Iguaçu em Foz do Iguaçu. Depos de 1098 km de estrada..'
  },
  {img : 'centro-de-itaquera-2.jpg',
  sub: 'Meu bairro',
  tit:'Conheça Itaquera como ele é.',
  cont:'Um bairro com dimensão e população de uma cidade, com várias cidades integradas..'
  }
  ]

  constructor() { }

  ngOnInit() {
  }

}
