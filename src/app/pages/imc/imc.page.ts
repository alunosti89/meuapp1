import { Component, OnInit } from '@angular/core';
import { Calculo } from 'src/app/diretiva/calculo';
import { analyzeAndValidateNgModules } from '@angular/compiler';

@Component({
  selector: 'app-imc',
  templateUrl: './imc.page.html',
  styleUrls: ['./imc.page.scss'],
})
export class ImcPage implements OnInit {
  imc: number = 0;
  condicao: string;
  altura: number;
  peso: number
  constructor() { }
  ngOnInit() {
  }
  // será chamado pelo botão no HTML
  calcular(){
    this.imc = Calculo.calcularImc(this.altura, this.peso);
    this.condicao = Calculo.informarImc(this.imc);

  }


}
