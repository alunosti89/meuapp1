import { Directive } from '@angular/core';

@Directive({
  selector: '[appCalculo]'
})
export class Calculo {
    static calcularImc(alt:number, peso: number):number{
        return (peso/(alt*alt));
    }
    static informarImc(imc: number):string{
        if(imc < 16)
            return "Magreza grave";
        else if(imc < 17) // 16.0 ~ 16.99999
            return "Magreza moderada";
        else if(imc < 18.5) // 17.0 ~ 18.499999999999
            return "Magreza Leve";
        else if(imc < 25) // 18.5 ~ 24.99999999999
            return "Saudável";
        else if(imc < 30) // 25.0 ~ 29.99999999
            return "Sobrepeso";
        else if(imc < 35)
            return "Obesidade Grau I";
        else if(imc < 40)
            return "Obesidade Grau II (Severa)";
        else 
            return "Obesidade Grau III (Móbida)"
    }


  constructor() { 
      console.log("passei aqui...");
  }

}
